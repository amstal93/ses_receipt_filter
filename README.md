<!-- BEGIN_TF_DOCS -->
# AWS\_SES\_RECEIPT\_FILTER

Terraform module to provision aws\_ses\_receipt\_filter resource.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.63.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.63.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ses_receipt_filter.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ses_receipt_filter) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cidr"></a> [cidr](#input\_cidr) | (Required) The IP address or address range to filter, in CIDR notation | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | (Required) The name of the filter | `string` | n/a | yes |
| <a name="input_policy"></a> [policy](#input\_policy) | (Required) Block or Allow | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | The SES receipt filter ARN |
| <a name="output_id"></a> [id](#output\_id) | The SES receipt filter name |

# Contributors

Created by [Viacheslav Kuzmenko](https://gitlab.com/slava-kuzmenko).
<!-- END_TF_DOCS -->