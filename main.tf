resource "aws_ses_receipt_filter" "this" {
  name   = var.name
  cidr   = var.cidr
  policy = var.policy
}
