variable "name" {
  description = "(Required) The name of the filter"
  type        = string
}

variable "cidr" {
  description = "(Required) The IP address or address range to filter, in CIDR notation"
  type        = string
}

variable "policy" {
  description = "(Required) Block or Allow"
  type        = string
}
